import { TestBed } from '@angular/core/testing';

import { RetroListService } from './retro-list.service';

describe('RetroListService', () => {
  let service: RetroListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RetroListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
