import { Injectable } from '@angular/core';
import {RetroItemModel} from "../models/retro-item.model";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RetroListService {
  private sadList: RetroItemModel[] = [{
    issue: 'A lot of homework',
    comments: [],
    isLiked: false
  },
    {
      issue: 'Few technical interviews',
      comments: [],
      isLiked: false
    }];
  private gladList: RetroItemModel[] = [{
    issue: 'English course',
    comments: [],
    isLiked: false
  },
    {
      issue: 'Interesting homework',
      comments: [],
      isLiked: false
    },
    {
      issue: 'Cool team',
      comments: [],
      isLiked: false
    }];
  private madList: RetroItemModel[] = [{
    issue: 'OPSWAT issues',
    comments: [],
    isLiked: false
  }];

  constructor() { }

  getSadList(): Observable<RetroItemModel[]>{
    return of(this.sadList);
  }

  getMadList(): Observable<RetroItemModel[]>{
    return of(this.madList);
  }

  getGladList(): Observable<RetroItemModel[]>{
    return of(this.gladList);
  }

  postGladItem(item: RetroItemModel){
    this.gladList.push(item);
  }
}
