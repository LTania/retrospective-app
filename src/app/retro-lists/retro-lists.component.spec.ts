import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RetroListsComponent } from './retro-lists.component';

describe('RetroListsComponent', () => {
  let component: RetroListsComponent;
  let fixture: ComponentFixture<RetroListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RetroListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetroListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
