import {Component, OnDestroy, OnInit} from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { RetroItemModel } from "../core/models/retro-item.model";
import { RetroListService } from "../core/services/retro-list.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-retro-lists',
  templateUrl: './retro-lists.component.html',
  styleUrls: ['./retro-lists.component.sass']
})
export class RetroListsComponent implements OnInit, OnDestroy{
  public itemToAdd = 'Add me';
  public commentToAdd = 'Add me';
  public subscriptions = new Subscription();
  public sadList: RetroItemModel[] = [];
  public gladList: RetroItemModel[] = [];
  public madList: RetroItemModel[] = [];
  public isSadComment: boolean = false;
  public isMadComment: boolean = false;
  public isGladComment: boolean = false;

  constructor(private retroService: RetroListService) { }

  ngOnInit(): void {
    this.subscriptions.add(this.retroService.getSadList().subscribe( list => this.sadList = list));
    this.subscriptions.add(this.retroService.getMadList().subscribe(list => this.madList = list));
    this.subscriptions.add(this.retroService.getGladList().subscribe(list => this.gladList = list));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onDrop(event: CdkDragDrop<RetroItemModel[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex, event.currentIndex);
    }
  }

  addItem(){
    this.retroService.postGladItem({
      issue: this.itemToAdd,
      isLiked: false,
      comments: []
    });
    this.itemToAdd = '';
  }

  addCommentBox(item: RetroItemModel, listType: string){
    if (listType === 'sad'){
      this.isSadComment = true;
    } else  if (listType === 'mad'){
      this.isMadComment = true;
    } else {
      this.isGladComment = true;
    }
  }

  addComment(item: RetroItemModel, list: RetroItemModel[]){
    list.forEach( listItem => {
      if(item === listItem){
        listItem.comments.push(this.commentToAdd);
      }
    })
    this.commentToAdd = '';
  }

  likeItem(item: RetroItemModel, list: RetroItemModel[]){
    list.forEach( listItem => {
      if (item === listItem) {
        listItem.isLiked = !listItem.isLiked;
      }
    })
  }

  likeItemSad(item: RetroItemModel){
    this.likeItem(item, this.sadList)
  }

  likeItemMad(item: RetroItemModel){
    this.likeItem(item, this.madList)
  }

  likeItemGlad(item: RetroItemModel){
    this.likeItem(item, this.gladList)
  }

  addCommentSad(item: RetroItemModel){
    this.isSadComment = false;
    this.addComment(item, this.sadList);
  }

  addCommentMad(item: RetroItemModel){
    this.isMadComment = false;
    this.addComment(item, this.madList);
  }

  addCommentGlad(item: RetroItemModel){
    this.isGladComment = false;
    this.addComment(item, this.gladList);
  }
}
